from cx_Freeze import setup, Executable
import os

PYQT5_DIR = "C:/Python34/lib/site-packages/PyQt5"

include_files = [
    "icon.png",
    "qmls/",
    "icons/",
    (os.path.join(PYQT5_DIR, "qml", "QtQuick.2"), "QtQuick.2"),
    (os.path.join(PYQT5_DIR, "qml", "QtQuick"), "QtQuick"),
    (os.path.join(PYQT5_DIR, "qml", "QtGraphicalEffects"), "QtGraphicalEffects"),
]

build_exe_options = {
    "packages": [
        "os",
        "PyQt5.QtGui",
        "PyQt5.QtNetwork",
        "PyQt5.QtQml",
        "PyQt5.QtQuick",
        "PyQt5.QtCore",
        "PyQt5.QtWidgets",
        "PyQt5.QtOpenGL",
    ],
    "excludes": ["tkinter"],
    "optimize": 2,
    "include_files": include_files
}

target = Executable("run.py",
                    base="Win32GUI",
                    icon="icon.ico"
                    )

setup(
    name="QMaterial",
    version="0.1",
    description="Test QT application with QML components",
    options={"build_exe": build_exe_options},
    executables=[target],

)