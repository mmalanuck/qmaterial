import os
from PyQt5 import QtQuick, QtCore, QtGui

__author__ = 'cjay'


class MaterialApp(QtQuick.QQuickView):
    """
    Главное окно
    """

    def __init__(self, app, title, qml_dir):
        super(MaterialApp, self).__init__()
        engine = self.engine()
        engine.addImportPath(qml_dir)

        self.setResizeMode(QtQuick.QQuickView.SizeRootObjectToView)
        self.setTitle(title)
        self.resize(1024, 768)

        self.app = app

        self.init_pallete()
        self.init_fonts()

        self.base_path = qml_dir

    def setSource(self, qml_path):
        file_path = os.path.join(self.base_path, qml_path)
        super(MaterialApp, self).setSource(QtCore.QUrl.fromLocalFile(file_path))

    def init_pallete(self):
        primary_colors = (
            "#e3f2fd", "#bbdefb", "#90caf9", "#64b5f6", "#42a5f5",
            "#2196f3", "#1e88e5", "#1976d2", "#1565c0", "#0d47a1",
            "#82b1ff", "#448aff", "#2979ff", "#2962ff"
        )

        secondary_colors = (
            "#fce4ec", "#f8bbd0", "#f48fb1", "#f06292", "#ec407a",
            "#e91e63", "#d81b60", "#c2185b", "#ad1457", "#880e4f",
            "#ff80ab", "#ff4081", "#f50057", "#c51162"
        )

        grey_colors = (
            "#fafafa", "#f5f5f5", "#eeeeee", "#e0e0e0", "#bdbdbd",
            "#9e9e9e", "#757575", "#616161", "#424242", "#212121",
            "#ff00ff", "#ff00ff", "#ff00ff", "#ff00ff"
        )

        self.add_pallete("p1", primary_colors)
        self.add_pallete("p2", secondary_colors)
        self.add_pallete("p", grey_colors)

    def init_fonts(self):
        app_fonts = QtGui.QFontDatabase()
        app_fonts.addApplicationFont(":/material/fonts/roboto/Roboto-Regular.ttf")
        app_fonts.addApplicationFont(":/material/fonts/roboto/Roboto-Light.ttf")
        app_fonts.addApplicationFont(":/material/fonts/roboto/Roboto-Medium.ttf")

    def add_pallete(self, prefix, colors):
        ctxt = self.rootContext()

        names = ("50", "100", "200", "300", "400", "500", "600", "700", "800", "900",
                 "a100", "a200", "a400", "a700")

        for n, val in enumerate(colors):
            ctxt.setContextProperty(prefix + "_" + names[n], val)

    def exit_code(self):
        self.app.exec_()
