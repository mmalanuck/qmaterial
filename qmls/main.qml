import QtQuick 2.0


import "std"
import "material"
import "editor_components"
import "kitchen"


Rectangle{
    id: root
	anchors.fill: parent

    KitchenSink{
        id: kitchen
        visible: true
        focus: true
    }
}