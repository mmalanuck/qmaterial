import QtQuick 2.0

import "../material"


Item{
	id: root
	width: 20
	height: 48-18
	Component.onCompleted:{
		var next = nextSibling(parent, this)
		var prev = prevSibling(parent, this)
		var lineHeight = prev.lineHeight

		height = lineHeight-18-4

		if (qmltypeof(prev, "Display2")){
			if (qmltypeof(next, "Subhead")){
				height = lineHeight-18
			}
		}
		if (qmltypeof(prev, "Display1")){
			if (qmltypeof(next, "Subhead")){
				height = lineHeight-18-3
			}
		}
		if (qmltypeof(prev, "Headline")){
			if (qmltypeof(next, "Body1")){
				height = lineHeight-18-4
			}
		}
		if (qmltypeof(prev, "Subhead")){
			if (qmltypeof(next, "Body1")){
				height = lineHeight-18-4
			}
		}

	}

	function qmltypeof(obj, className) { 
	  var str = obj.toString();
	  return str.indexOf(className + "(") == 0 || str.indexOf(className + "_QML") == 0;
	}

	function indexOfChild (item, child) {
	    var ret = -1;
	    if (item && child && "children" in item) {
	        for (var idx = 0; ret < 0 && idx < item.children.length; idx++) {
	            if (item.children [idx] === child) {
	                ret = idx;
	            }
	        }
	    }
	    return ret;
	}

	function prevSibling (item, child) {
	    return (item.children [indexOfChild (item, child) -1] || null);
	}

	function nextSibling (item, child) {
	    return (item.children [indexOfChild (item, child) +1] || null);
	}
}


