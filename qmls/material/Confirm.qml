import QtQuick 2.0
import QtQuick.Window 2.2

import "../std"

Dialog{
	id: root
	property alias text: txt.text
	property alias title: title_txt.text
	property alias accept_label: btn_agree.text
	property alias reject_label: btn_disagree.text
	signal accepted()
	signal rejected()

	actions: Row{
        FlatButton{
        	id: btn_disagree
        	color: p1_500
            text: "DISAGREE"
            onClicked: root.rejected()
        }
        FlatButton{
        	id: btn_agree
        	color: p1_500
            text: "AGREE"
            onClicked: root.accepted()
        }
        
    }

	content: [		
		Column{
			Title{
				id: title_txt
				width: root.width  - 48
				wrapMode: Text.Wrap				
				visible: text.length>0
			}
			Item{
				width: 15
				height: 8
				visible: title_txt.text.length>0
			}
			Text{ 
				id: txt
				width: root.width  - 48
				opacity: 0.54
				font.pixelSize: 16
				lineHeight: 1.15
				wrapMode: Text.Wrap
				text: text
			}	
		}
		
	]
}