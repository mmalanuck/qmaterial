import QtQuick 2.0
import "../std"
import "../"



Item{	
	id: root	
	height: parent.height
	width: 250
	x: position=="left" ? showed? 0 : -width - 10 :  showed? 0 : width + 10

	property alias title: subhead_text.text
	property alias content: content_item.children   
	property bool showed : true 
	property string position : "left"


	Shadow{
		target: rect
		size: 2
	}
	Rectangle{
		id: rect
		color: p_300
		anchors.fill: parent	
	}


	Item{
		id: title		
		height: 64
		anchors.top: root.top
		anchors.left: root.left
		anchors.right: root.right			

		Subhead{
			id: subhead_text
			color: "black"
			x: root.position == "left" ? 16 : 16 + 32
			text: "Title"
			anchors.bottom: parent.bottom
			anchors.bottomMargin: 26
		}

		Item{	
			id: icon_item					
			
			width: 36
			height: 36
			anchors.bottom: parent.bottom
			anchors.bottomMargin: 12

			IconButton{
				anchors.verticalCenter: parent.verticalCenter
				// background: p_500		
				path: root.position == "left" ? "icons/chevron-left.svg" : "icons/chevron-right.svg"
				// color: "black"

				onClicked: {
					root.showed = ! root.showed
					
				}
			}
			Component.onCompleted:{				
				if (root.position == "left"){
					icon_item.anchors.right = icon_item.parent.right
					icon_item.anchors.rightMargin =  8					
				}else{
					icon_item.anchors.left = icon_item.parent.left
					icon_item.anchors.leftMargin = 8
				}
			}

		}
	}

	Divider{
		id: toolbar
		width: rect.width
		y: 64
		z: 1
	}

	Item{
		y: 64
		width: parent.width
		height: parent.height - 64
		id: content_item
	}

	Behavior on x { SmoothedAnimation { velocity: 800 } }

        
}