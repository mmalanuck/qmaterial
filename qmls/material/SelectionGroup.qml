import QtQuick 2.0

Item{
	id: root
	visible:true
	property var children: new Array()
	property var selected: new Array()
	property int selectedCount: 0


	/**
	 * Убирает элемент из списка выделенных
	 */
	function deselect_item(item){
		var index = root.selected.indexOf(item)
		if (index != -1){
			root.selected.splice(index, 1)	
		}
		root.selectedCount = root.selected.length
	}

	/**
	 * Добавляет элемент в список выделенного
	 */
	function set_selection(item, mouse_modifier){				
		if (mouse_modifier==Qt.ControlModifier){
			var index = root.selected.indexOf(item)
			if (index==-1){
				root.selected.push(item)				
			}else{
				root.selected.splice(index,1)
			}
		}else{
			root.selected.length=0
			root.selected.push(item)
		}
		root.selectedCount = root.selected.length

		for(var i=0; i<children.length; i++){
			if (children[i] && (root.selected.indexOf(children[i]) ==-1)){
				children[i].deselect_item()
			}
		}
	}

	/**
	 * регистрируем элемент в системе выделения
	 */
	function add_item(item){
		if (children.indexOf(item) == -1){
			children.push(item)	
		}
	}

	/**
	 * удаляем элемент из системы выделения
	 */
	function remove_item(item){
		var index = children.indexOf(item)
		if ( index!= -1){
			children.splice(index, 1)
		}
		deselect_item(item)
	}
	
	Component.onDestruction: console.log("Des")
}