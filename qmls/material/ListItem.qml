import QtQuick 2.0
import "../std"

Item{
	id: root
	property alias title: txt.text
	property alias icon: icon_place.children
	property string image: ""
	property bool double_line: false
	property string description
	property alias secondary: secondary_place.children
	// property color color: "white"
	property bool active: false
	
	property bool has_left_offset : image!="" ? true : icon.length>0 ? true : false

	width: parent.width
	
	height: txt_desc.lineCount>1? 88 :description!=""? 72 : 48

	Rectangle{
		width: secondary_place.children.length>0 ? parent.width - 56 : parent.width
		height: parent.height
		color:  "black" //root.color
		opacity: root.active? 0.08 : mouse_primary.containsMouse? 0.08 : 0

		MouseArea{
			id: mouse_primary
			anchors.fill: parent
			hoverEnabled: true
			onClicked: console.log("primary")
		}
	}
	Item{
		width: secondary_place.children.length>0 ? 56 : 0
		height: parent.height
		anchors.right: parent.right
		// color:  "green" //root.color

		MouseArea{
			anchors.fill: parent
			onClicked: console.log("secondary")
		}
	}
	
	Image{				
		x: 16
		sourceSize.width: 40
		sourceSize.height: 40
		source: root.image
		anchors.verticalCenter: parent.verticalCenter
	}	
	
	Item{
		anchors.verticalCenter: parent.verticalCenter
		x: 16
		width: 40
		height: 40
		id: icon_place
	}


	TextMedium{
		id: txt		
		clip: true		
		
		width: secondary_place.children.length>0? root.has_left_offset ?  root.width - 72 - 56 :root.width - 72 :root.has_left_offset ?  root.width - 72 - 16 :root.width - 32
		height: 20
		x: root.has_left_offset ? 72 : 16
		y: root.height==56 ? 11 : 15

	}
	Item{
		anchors.top: txt.bottom
		anchors.left: txt.left
		clip: true
		width: secondary_place.children.length>0? root.has_left_offset ?  root.width - 72 - 56 :root.width - 72 :root.has_left_offset ?  root.width - 72 - 16 :root.width - 32
		height: txt_desc.lineCount<2? 15 : 36

		TextMedium{						
			id: txt_desc
			width: parent.width
			wrapMode: Text.WrapAnywhere
			text: root.description
			font.pixelSize: 14
			opacity: 0.7
		}

	}

	
	Item{
		width: 56
		height: root.height
		anchors.right: root.right

		Item{
			anchors.fill: parent
			anchors.leftMargin: 4
			anchors.rightMargin: 16
			id:secondary_place
			// color: "red"
		}
		
	}
	


}
