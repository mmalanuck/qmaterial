import QtQuick 2.0

import "../std"



Item{
	id: root
	property string title: "Безымянный"
	property bool disabled: false
	property bool active : false
	// property bool colored: parent.colored
	property int tab_size: parent.tab_size
	property var target

	signal clicked()

	width: tab_size
	height: 48

	clip: false


	

	TextButton{
		id: txt
		opacity: active? true : mouse.containsMouse ? 1 : 0.7
		text: root.title	
		width: root.tab_size
		horizontalAlignment: Text.AlignHCenter
		color: "white"
		y : lineCount>1 ? 7 : 14
		wrapMode: Text.WordWrap
	}

	

	Ripple{
		id: ripple
		color: txt.color
		width: 38
		height: 38
		// anchors.centerIn: parent
	}

	MouseArea {
		id: mouse
        anchors.fill: parent
        hoverEnabled: true
        enabled: !disabled
        onClicked: {
        	if (! active){
        		root.clicked(root)
        		select_tab()	
        		root.active = true
        		
	        	ripple.anim()
        	}
        	

        }
    }

    Component.onCompleted:  {
    	if (target){
    		target.visible = false
    	}
    	if (active) {
    		select_tab() 
		}
    }

    function select_tab(){    	
    	parent.on_select(root)
    	if (target){
    		target.visible = true
    	}
    }

    function hide(){
    	root.active = false
    	if (target){
    		target.visible = false
    	}
    }

}