import QtQuick 2.0


import "../std"
import "../material"



Dialog{
    id: root

    property var creations
    property var references
    property var synonims
    

    property var current_content : creations_content


    title: "Свойства"
    visible: true
    width: 56*10
    height: 600



    header: Tabs{            

        content:[
            TabItem{ 
                title: creations ? "Описание (" + creations.length + ")" : "Описание"
                active: true; target: tab_content_1 ; onClicked: root.current_content = creations_content
            },
            TabItem{ title: references? "Ссылки (" + references.length + ")" : "Ссылки"
                target: tab_content_2; onClicked: root.current_content = references_content
            },
            TabItem{ title: synonims ? "Формы слов (" + synonims.length + ")" : "Формы слов"
                target: tab_content_3; onClicked: root.current_content = synonims_content
            },
            TabItem{ title: "Графики"; target: tab_content_3; onClicked: root.current_content = synonims_content}
        ]
    }


    content : Item{
        // anchors.fill: parent
        id: cntnt_itm
        width: root.width - 48
        height: (current_content.height < (root.height-260) ? current_content.height : (root.height-260))+8

        Behavior on height{ SmoothedAnimation{velocity: 600}}
        clip: true
        // clip: false

        Column{
            id: clmn
            // width: cntnt_itm.width
            width: 600


            Flickable {    
                // id:  flick_panel
                id: tab_content_1
                // anchors.fill: parent
                contentWidth: creations_content.width
                contentHeight: creations_content.height
                width: cntnt_itm.width
                height:  cntnt_itm.height
                
                clip: true

                flickableDirection: Flickable.VerticalFlick
                boundsBehavior: Flickable.StopAtBounds


                Column{
                    id: creations_content
                    width: cntnt_itm.width
                    // height: 1200
                    // width: 600

                    Repeater{   
                        model: creations

                        
                        InfoItem{
                            width: cntnt_itm.width
                            url: model.modelData.url
                            content: model.modelData.node                            
                        }
                    }
                }
            }            

            Flickable {    
                // id:  flick_panel
                id: tab_content_2
                // anchors.fill: parent
                contentWidth: references_content.width
                contentHeight: references_content.height
                width: cntnt_itm.width
                height:  cntnt_itm.height
                // height: 200
                // width: 400
                clip: true

                flickableDirection: Flickable.VerticalFlick
                boundsBehavior: Flickable.StopAtBounds

                Column{
                    id: references_content
                    width: cntnt_itm.width
                    // height: 1200

                    Repeater{   
                        model: references

                        LinkItem{                            
                            url: model.modelData.url
                            pos: model.modelData.pos
                            width: cntnt_itm.width
                            onClicked: dispatcher.open_file(url, pos)
                        }
                    }
                }
            }            

            Flickable {    
                // id:  flick_panel
                id: tab_content_3
                // anchors.fill: parent
                contentWidth: synonims_content.width
                contentHeight: synonims_content.height
                width: cntnt_itm.width
                height:  cntnt_itm.height
                // height: 200
                // width: 400
                // clip: true

                flickableDirection: Flickable.VerticalFlick
                boundsBehavior: Flickable.StopAtBounds

                
                Column{
                    id: synonims_content


                    SynonimsPanel{
                        id: itm_synonyms
                        width : cntnt_itm.width
                        items: root.synonims                        
                    }
                }
            }
        }
        ScrollBar{
            visible:tab_content_1.visible 
            target: tab_content_1
        }   
        ScrollBar{
            visible:tab_content_2.visible 
            target: tab_content_2
        }  
        ScrollBar{
            visible:tab_content_3.visible 
            target: tab_content_3
        }    
    }

    actions: Row{                            
        FlatButton{
            text: "ЗАКРЫТЬ"
            height: 36
            onClicked: node_info_dialog.visible = false
        }
    }

    MouseArea {
        // Эта штука нужна только для перехвата скролла
        // чтобы списки под этими элементами не скролились

        anchors.fill: parent
        acceptedButtons: Qt.MiddleButton | Qt.RightMouse
    }
}