import QtQuick 2.0


import "../std"
import "../material"

Item{
    id: root
    anchors.margins: 24
    height: clmn_synonims.height
    property var items
    
    property var base
    property var localBase

    
    ListModel{
        id: localItems
    }
    ListModel{
        id: originalItems
    }

    Column{
        id: clmn_synonims
        width: parent.width
        
        Title {
            text: "Формы для слова '" + localBase + "'"
        }
        Item{
            width: 20
            height: 20
        }
        Item{
            width: parent.width
            height: flw.childrenRect.height
            
            Flow{                          
                id: flw                                      
                spacing: 8

                width: parent.width                                    
                Repeater{
                    id: rptr
                    model: localItems
                    Tag{
                        title: model.value
                        selection_group: synonims_selection_group
                        onRemovedChanged:{
                            if (removed==true){
                                localItems.setProperty(index, "removed", true)
                            }else{
                                localItems.setProperty(index, "removed", false)
                            }
                            has_changes()
                        }
                        onTitleChanged:{
                            localItems.setProperty(index, "value", title)
                            has_changes()
                        }
                    }

                    onItemAdded:{
                        if (root.base == item.title){
                            item.inverted = true
                        }
                    }
                }
                IconButton{
                    color: p1_500
                    path: "icons/plus.svg"
                    onClicked: createNewItem()
                }
            }
            SelectionGroup{
                id: synonims_selection_group
                // onSelectedChanged: console.log("cchhh")
            }
        }
        Item{
            width: 24
            height: 24
        }
        Item{
            width: 100
            height: 32
            Row{
                RaisedButton{
                    text: "Сделать базовой формой"
                    disabled: !(synonims_selection_group.selectedCount==1)
                    onClicked: makeBaseForm()
                }                                    
                RaisedButton{ 
                    id: save_btn                                       
                    text: "Сохранить"
                    disabled: false
                    onClicked: root.save_items()
                }
            }
        }
    }      

    onItemsChanged: prepeare_items()

    /**
     * Добавляем новую форму слова
     */

    function makeBaseForm(){
        for (var i=0; i <synonims_selection_group.children.length; i++){
            synonims_selection_group.children[i].inverted = false
        }
        var item = synonims_selection_group.selected[0];                                            
        item.inverted = true 
        root.localBase = item.title
        has_changes()
    }


    /**
     * Добавляем новую форму слова
     */
    function createNewItem(){
        localItems.append({"value": "No title", "removed": false})
    }

    /**
     * Заполняет ListModels на основе переданного свойства items
     */
    function prepeare_items(){
        localItems.clear()
        originalItems.clear()
        for (var i in items){            
            localItems.append({"value":items[i], "removed":false})
            originalItems.append({"value":items[i], "removed":false})
        }
        base = items[0]
        localBase = base
    }

    /**
     * Отправляем диспетчеру изменения форм слов
     */
    function save_items(){
        var items = []
        var pos = 0
        while(pos<localItems.count){
            var item = localItems.get(pos)
            if (item.removed == true){
                localItems.remove(pos,1)
                continue
            }else{
                pos+=1
            }
        }
        dispatcher.replace_synonyms(localBase, items, base)
        copy_to_original(localItems ,originalItems)
        has_changes()
    }

    /**
     * Проверяет, равны ли по значениям listModel
     */
    function compare_listModels(a,b, comparator){
        if (a.count != b.count) return false

        for (var i=0; i<a.count; i++){
            var has_item = false
            var a_item = a.get(i)            
            for (var j=0; j< b.count; j++){
                var b_item = b.get(j)
                
                if (comparator(a_item, b_item)){
                    has_item = true
                    // console.log("Comp", a_item.value)
                    break
                }
            }
            if (!has_item){
                return false
            }
        }
        return true
    }

    /**
     * Проверяет, были ли сделаны изменения
     */
    function has_changes(){
        var res =  true
        var count = 0

        if (localBase != base){
            save_btn.disabled = false
            return
        }
                
        if (originalItems.count == localItems.count){
            if (compare_listModels(localItems, originalItems, function(a,b){
                if (a.value!= b.value) return false
                if (a.removed!= b.removed) return false
                return true
            })){
                res = false
            }
        }
        save_btn.disabled = !res
    }

    /**
     * копируем из одного ListModel в другой
     */
    function copy_to_original(source, target){
        target.clear()
        var item = null;
        for(var i=0; i<source.count; i++){
            item = source.get(i)
            if (item.removed==false){
                target.append({"value":item.value, "removed":false})
            }
        }        
    }
}