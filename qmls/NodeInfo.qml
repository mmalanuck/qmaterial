import QtQuick 2.1

Rectangle{
	id: root

    height: childrenRect.height
    width: 600
    color: "#3e4850"
    
    property string url
    property string description

    


    Rectangle{
        height: childrenRect.height+12
        width: parent.width                    
        color: "#3e4850"
        y: 20
        anchors.leftMargin: 30
        
        Text {
            y: 6
            x: 6
            width: parent.width-12
            text: root.description
            font.pointSize: 10
            color: "#757f87"                    
            wrapMode: Text.WordWrap
            renderType: Text.NativeRendering
        }    

    }
    Rectangle{
            id: id_file_panel
            width: parent.width
            height: 20
            color: "#283036"
            y: 0
            opacity: 0

            MyButton{
                text: "Перейти к файлу"
            }

            states: [
                State { 
                    name: "opened"
                    PropertyChanges {
                        target: id_file_panel
                        y:20
                        opacity: 1
                    }
                    PropertyChanges{
                        target: id_label
                        color: "#283036"
                    }
                },
                State { 
                    name: ""
                    PropertyChanges {
                        target: id_file_panel
                        y:5
                        opacity: 0
                    }
                    PropertyChanges{
                        target: id_label
                        color: "#353d44"
                    }
                }
            ]

            transitions: [
                Transition{
                    from: "opened"
                    to: ""
                    NumberAnimation{ target: id_file_panel; property: "y"; easing.type: Easing.InBack; duration: 100}
                },
                Transition{
                    from: ""
                    to: "opened"
                    NumberAnimation{ target: id_file_panel; property: "y"; easing.type: Easing.InBack; duration: 100}
                }

            ]
    }
    Rectangle{
        id: id_label
        color: "#353d44"                        
        height: 20
        width: parent.width
        anchors.top: parent.top
        
        Text {
            text: root.url                            
            font.pointSize: 11                    
            color: "#2d8b9f"                            
            renderType: Text.NativeRendering
        }
        
        MouseArea{
            anchors.fill: parent
            onClicked: id_file_panel.state == 'opened' ? id_file_panel.state = '' : id_file_panel.state = 'opened' ;
        }
    }
}