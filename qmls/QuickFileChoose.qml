import QtQuick 2.1

Rectangle {    
    id: root
    objectName: "root"    
    color: "#283036"
    
    signal sizeChanged(var size)
    signal locationChoiced(var item)
    signal selected(string url, int pos)

    ListModel { id: dataModel   }      //     {url, pos} 

    Flickable {
        width: parent.width
        height: parent.height
        clip: true
        boundsBehavior: Flickable.DragAndOvershootBounds 
        
        contentWidth: childrenRect.width


        Column {
            id: column_id
            x:6        
            y: 6
            width: root.width - 12            
            spacing: 12
            height: childrenRect.height
            

            Repeater {
                model: dataModel
                delegate: QuickFileInfo{
                    width: root.width
                    url: dataModel.get(index).url
                    pos: dataModel.get(index).pos
                }
                onItemAdded: {                    
                    item.clicked.connect(function(url, pos){                        
                        root.selected(url, pos);
                    });
                }
            }

            onHeightChanged: root.sizeChanged(get_size())
        }
    }

    

    function set_items(items, width){
        root.width = width
        dataModel.clear()
        dataModel.append(items)
    }    

    function get_size(){
        return {"width":column_id.width + 12, "height" : column_id.height + 22}
    }
}