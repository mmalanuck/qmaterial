import QtQuick 2.0

import "../material"
import "../editor_components"

Item{
	id: root
	width: 500	

	Item{
		x: 20
		width: parent.width
		height: 500

		Tabs{
		   content:[
		       TabItem{ title: "Page1"; target: tab_content_1},
		       TabItem{ title: "Page2"; target: tab_content_2},
		       TabItem{ title: "Page3"; target: tab_content_3}
		   ]
		}		

		Rectangle{
			y: 48
			color: p_200
			width: root.width
			height: 300
			Rectangle{
				color: "red"
				width: 100
				height: 100
				anchors.centerIn: parent
				id: tab_content_1

				Text{
					y: -30
					text: "First page"
				}
			}
			Rectangle{
				color: "green"
				width: 100
				height: 100
				id: tab_content_2
				anchors.centerIn: parent

				Text{
					y: -30
					text: "Second page"
				}
			}
			Rectangle{
				color: "blue"
				width: 100
				height: 100
				id: tab_content_3
				anchors.centerIn: parent

				Text{
					y: -30
					text: "Third page"
				}
			}
		}
	}
	


	

}