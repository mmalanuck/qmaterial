import QtQuick 2.0

import "../std"
import "../material"
import "../editor_components"

Item{
	id: root
	width: 800	
	height: 500	

		Flow{
			anchors.fill: parent
			// anchors.margins: 4
        	spacing: 18

			Repeater{
				model: 24
				Item{
					width: 60
					height: 60
					Shadow{
						size: modelData
						target: r1
					}
					Rectangle{
						id: r1
						width: 60
						height: 60
						color: p1_500
						anchors.verticalCenter: parent.verticalCenter
						anchors.horizontalCenter: parent.horizontalCenter	
						Text{
							anchors.centerIn: parent
							text: modelData
						}	
					}
				}
			}
			
		}
	Item{
		anchors.centerIn: parent
		Shadow{
			id: shd
			size: 12
			target: itm
			NumberAnimation on size {
		        running: true
		        from: 1; to: 24
		        loops: -1
		        duration: 1000*24*0.2
		    }
		}
		Item{
			id: itm
			width: 60
			height: 60
			// color: p1_500
			Text{
				anchors.centerIn: parent
				text: shd.size
			}	

		}
	}
	
}