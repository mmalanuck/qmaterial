import QtQuick 2.0

import "../std"
import "../material"
import "../editor_components"

Item{
	id: root
	width: 800
	height: 500
	// anchors.fill: parent

	Column{
		x: 20
		width: 320

		Display2{	
			text: "Others have failed, I will not."
			width: parent.width
		}
		HeaderCorrector{}
		Subhead{
			text: "Science, my boy, is made up of mis- takes, but they are mistakes which it is usefull to make, because they lead little by little to the truth."
			width: parent.width
		}
	}

	Column{
		x: 380
		width: 320

		Display1{	
			text: "Others have failed, I will not."
			width: parent.width
		}
		HeaderCorrector{}
		Subhead{
			text: "Science, my boy, is made up of mis- takes, but they are mistakes which it is usefull to make, because they lead little by little to the truth."
			width: parent.width
		}
	}

	Column{
		y: 240
		x: 20
		width: 320

		Headline{	
			text: "Others have failed, I will others have failed, I will not."
			width: parent.width
		}
		HeaderCorrector{}
		Body1{
			text: "Science, my boy, is made up of mis- takes, but they are mistakes which it is usefull to make, because they lead little by little to the truth."
			width: parent.width
			wrapMode: Text.Wrap
		}
	}

	Column{
		y: 240
		x: 380
		width: 320

		Subhead{	
			text: "Others have failed, I will not. Others have failed, I will not."
			width: parent.width
		}
		HeaderCorrector{}
		Body1{
			text: "Science, my boy, is made up of mis- takes, but they are mistakes which it is usefull to make, because they lead little by little to the truth."
			width: parent.width
			wrapMode: Text.Wrap
		}
	}

	Column{
		y: 410
		x: 20
		width: 300

		Subhead{
			text: "The Hatter was the first to break the silence. 'What day of the month is it?' he said, turning to Alice: he had taken his watch out of his pocket, and was looking at it uneasily, shaking it every now and then, and holding it to his ear."
			width: parent.width
			wrapMode: Text.Wrap
		}
	}
	Column{
		y: 410
		x: 380
		width: 300

		
		Body2{
			text: "The Hatter was the first to break the silence. 'What day of the month is it?' he said, turning to Alice: he had taken his watch out of his pocket, and was looking at it uneasily, shaking it every now and then, and holding it to his ear."
			width: parent.width
			wrapMode: Text.Wrap
		}
	}
	

}