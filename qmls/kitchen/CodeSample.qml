import QtQuick 2.0

import "../std"
import "../material"
import "../editor_components"

Item{
	id: root
	width: 200
	height: 120


	Rectangle{
		id: rect	
		color :  root.dark ? p_700 : p_50 
		width: txt.contentWidth + 16
		height: txt.contentHeight + 16
		radius: 2


		Text{
			id: txt
			x: 8
			y: 8
			font.pixelSize: 11
			color: root.dark ? p_50 : p_800
		}
			
	}
	
	// clip: true		
	property alias text: txt.text
	property bool dark: false
	
	

}