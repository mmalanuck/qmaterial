import QtQuick 2.0
import "../material"
	

Item{			
	id: root	
	height: 48	
	

	Rectangle{
		anchors.fill: parent
		color: is_selected ? p1_500 : is_modified ? p1_100 : p_50
		visible : is_selected || is_modified
		opacity: is_selected ? 1 : 0.4
	}

	Item{
		anchors.fill: root
		anchors.leftMargin: 18 * level + 6

		Icon{
			color: is_selected ? "white" : "black"
			path: is_open ? "icons/file.svg" : "icons/file.svg"
			anchors.verticalCenter: parent.verticalCenter
			opacity: is_selected ? 1.0 : 0.3
		}					

		Text{
			x: 18+18
			font.pixelSize: 13
			text: name			
			color: is_selected ? "white" : "black"
			anchors.verticalCenter: parent.verticalCenter
			opacity: is_selected ? 1.0 : 0.7
			visible: !is_modified
		}
		Caption{
			x: 18+18
			font.pixelSize: 13
			text: name + "*"
			color: is_selected ? "white" : "black"
			anchors.verticalCenter: parent.verticalCenter
			opacity: is_selected ? 1.0 : 0.7
			visible: is_modified
		}
	}
	
	

	MouseArea{
		anchors.fill: parent
		onClicked: {
			onSelect(root.parent)
		}
	}
}
