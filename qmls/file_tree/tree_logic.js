function find_object_by_name(name, obj){
  // рекурсивно ищет объект с определённым именем
  if (! obj) return null
  
  if (obj.objectname == name){
    return obj;
  }
  
  var i;
  var item = null;
  for (i in obj.children){
    item = obj.children[i];
    if (name == item.objectName){
      // элемент найден
      return item;
    }      
  }
  
  // искать элемент во вложениях
  var result;
  for (i in obj.children){
    item = obj.children[i];
    result = find_object_by_name(name, item);
    if (result){
      break
    }      
  }
  return result;
    
} 


function open_folders_for_path(loader, path, onSuccessCallback){    
    // раскрывает папки, соответствующие пути.
    // Если находит конечный элемент, выполенняет над ним onSuccessCallback(item)

    if (path.indexOf(loader.path)==0){        

        if (loader.is_dir){
          loader.is_open = true;                    
        }else{
          loader.is_selected = true
        }

        // если конечный элемент найден
        if (path == loader.path){          
          if (onSuccessCallback){
            onSuccessCallback(loader);
          }          
          return;
        }

        if (loader.is_dir){
          // ищем во вложенных папках          
          var repeater = find_object_by_name("items_repeater", loader.item);
          if (repeater){            
            var i;
            for(i=0; i<repeater.count; i++){
              open_folders_for_path(repeater.itemAt(i), path, find_opened_node);
            }
          }
        }
        
      }
  }