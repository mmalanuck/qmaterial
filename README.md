#Qml Material UI

Код, помещённый в этом репозитории, является следствием попыток создать пользовательский интерфейс для десктопных
приложений на языке python  с использованием технологии QML.
Если назвать этот код проектом, то предупреждаю заранее, что проект так и не был завершён хоть до какого-либо
логического окончания. Возможно вам удастся адаптировать его под свои нужды, но на что-то серьёное рассчитывать не стоит.


### Для чего выложен этот код? ###

Только для того, чтобы показать, как можно делать сложные пользовательские компоненты на python c технологией QML.

### Как использовать ###

К сожалению, я пока не добрался до того, как можно оформлять набор своих компонентов в переносимые/подключаемые библиотеки. Сейчас же они
хранятся внутри директории *qmls*. Следите за тем, чтобы в этой папке были все необходимые qml-компоненты.

Для того, чтобы использовать, создайте объект MaterialApp, передав в его конструктор путь к
директории с qml-компонентами, а затем укажите основной qml-файл с интерфейсом:

```python
import os
import sys

from PyQt5 import QtGui
from kb23.material_ui.app import MaterialApp

if __name__ == "__main__":
    # получаем путь к директории с qml-компонентами
    dir_path = os.path.dirname(os.path.abspath(__file__))
    qml_dir = os.path.join(dir_path, "qmls")

    app = QtGui.QGuiApplication(sys.argv)

    window = MaterialApp(app, "MaterialUI Kitchensink", qml_dir)
    window.setSource("main.qml")    # указываем основной qml-файл
    window.show()

    sys.exit(window.exit_code())
```

А вот как может выглядеть основной qml-файл:

```qml
import QtQuick 2.0


import "std"
import "material"
import "editor_components"
import "kitchen"


Rectangle{
    id: root
	anchors.fill: parent

    KitchenSink{
        id: kitchen
        visible: true
        focus: true
    }
}
```

В предоставленном qml-файле находится компонент KitchenSink, находящийся внутри прямоугольника (прямоугольник заполняет всю доступную область и является контейнером).
Сам же компонент KitchenSink импортируется из директории "kitchen" благодаря команде `import "kitchen"`.

А вот как выглядит содержимое файла kitchen/KitchenSink.qml:

```qml
import QtQuick 2.0

import "../std"
import "../material"
import "../editor_components"

Dialog{
    id : root
    title: "Свойства"
    visible: true
    height: parent.height - 40
    hide_on_lost: false

    // property var current_content : creations_content

    header: Tabs{
        content:[
            TabItem{ title: "Button"; target: tab_content_1},
            TabItem{ title: "Cards";  active: true;  target: tab_content_2},
            TabItem{ title: "Dialog"; target: tab_content_3},
            TabItem{ title: "Confirm"; target: tab_content_10},
            TabItem{ title: "Alert"; target: tab_content_11},
            TabItem{ title: "Paper"; target: tab_content_4},
            TabItem{ title: "Navigator"; target: tab_content_5},
            TabItem{ title: "Lists"; target: tab_content_6},
            TabItem{ title: "Selections"; target: tab_content_7},
            TabItem{ title: "Shadow";  target: tab_content_8},
            TabItem{ title: "TextFields";  target: tab_content_9},
            TabItem{ title: "Divider";  target: tab_content_12},
            TabItem{ title: "Toolbar"; target: tab_content_13},
            TabItem{ title: "Texts"; target: tab_content_14},
            TabItem{ title: "Texts line height";  target: tab_content_15},
            TabItem{ title: "Tabs"; target: tab_content_16}
        ]
    }
    content :  Item{
        width: 500
        height: 500

        SampleButtons{
            id: tab_content_1
        }
        SampleCards{
            id: tab_content_2
        }
        SampleDialog{
            id: tab_content_3
        }
        SamplePaper{
            id: tab_content_4
        }
        SampleNavigator{
            id: tab_content_5
        }
        SampleLists{
            id: tab_content_6
        }
        SampleSelections{
            id: tab_content_7
        }
        SampleShadow{
            id: tab_content_8
        }
        SampleTextFields{
            id: tab_content_9
        }
        SampleConfirm{
            id: tab_content_10
        }
        SampleAlert{
            id: tab_content_11
        }
        SampleDivider{
            id: tab_content_12
        }
        SampleToolbar{
            id: tab_content_13
        }
        SampleText{
            id: tab_content_14
        }
        SampleTextHeight{
            id: tab_content_15
        }
        SampleTabs{
            id: tab_content_16
        }
    }

    actions: Row{

    }

    MouseArea {
        // Эта штука нужна только для перехвата скролла
        // чтобы списки под этими элементами не скролились

        anchors.fill: parent
        acceptedButtons: Qt.MiddleButton | Qt.RightMouse
    }
}
```

Для первого знакомства должно быть достаточно.

Так же доступно (устаревшее) описание некоторых компонентов **[по этой ссылке](https://mmalanuck.bitbucket.io/material_ui/)**.